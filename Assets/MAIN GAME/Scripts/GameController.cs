using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using DG.Tweening;
using MoreMountains.NiceVibrations;
using UnityEngine.EventSystems;
using FluffyUnderware.Curvy.Controllers;
using FluffyUnderware.Curvy;
using Cinemachine;

public class GameController : MonoBehaviour
{
    [Header("Variable")]
    public static GameController instance;
    public int maxLevel = 100;
    public bool isStartGame = false;
    public List<Color> listColors = new List<Color>();
    int currentColor;
    public Color themeColor;
    int maxPlusEffect = 0;
    public List<GameObject> levelList = new List<GameObject>();
    public List<CurvySpline> pathList = new List<CurvySpline>();
    public List<float> levelSizes = new List<float>();
    int currentPath = 0;
    public SplineController splineMove;
    public SplineController splineGuide;
    public bool isEffect = false;
    public float speed;
    public static int currentSkin = 0;

    [Header("UI")]
    public GameObject winPanel;
    public GameObject losePanel;
    public Slider levelProgress;
    public Text currentLevelText;
    int currentLevel;
    public Text bestScoreText;
    public Text moneyText;
    public static int score;
    public static int money;
    public Canvas canvas;
    public GameObject startGameMenu;
    public GameObject inGameMenu;
    public InputField levelInput;
    public Image splash;
    public Text openChestText;
    public Sprite openChestSprite;
    public GameObject closeChest;

    [Header("Objects")]
    public GameObject plusVarPrefab;
    public GameObject conffeti;
    GameObject conffetiSpawn;
    GameObject obstacle;
    public GameObject effect;
    public GameObject guide;
    public float radius;
    public GameObject walls;
    public GameObject ball;
    public List<Renderer> listTargetSkin = new List<Renderer>();
    public Renderer underSkin;
    public List<Texture> listSkin = new List<Texture>();
    public List<Texture> listUnderSkin = new List<Texture>();
    public Animator animator;
    public List<Transform> listCars = new List<Transform>();
    public CinemachineVirtualCamera directorCamera;

    private void OnEnable()
    {
        //PlayerPrefs.DeleteAll();
        Application.targetFrameRate = 60;
        instance = this;
        splineMove = GetComponent<SplineController>();
        splineGuide = guide.GetComponent<SplineController>();
        // foreach(var item in listTargetSkin)
        // {
        //     item.material.mainTexture = listSkin[currentSkin];
        // }
        // if(currentSkin <= 5)
        // {
        //     underSkin.material.mainTexture = listUnderSkin[currentSkin];
        // }
        currentSkin++;
        StartCoroutine(delayStart());
    }

    IEnumerator delayStart()
    {
        currentLevel = PlayerPrefs.GetInt("currentLevel");
        levelList[currentLevel].SetActive(true);
        foreach (Transform child in levelList[currentLevel].transform.GetChild(0))
        {
            pathList.Add(child.GetComponent<CurvySpline>());
        }
        splineMove.Spline = pathList[currentPath];
        yield return new WaitForSeconds(0.01f);
        themeColor = listColors[Random.Range(0, listColors.Count)];
        currentColor = 0;
        // GetComponent<Renderer>().material.color = themeColor;
        transform.localScale = Vector3.one * levelSizes[currentLevel];
        radius = transform.localScale.x;

        currentLevelText.text = currentLevel.ToString();
        score = PlayerPrefs.GetInt("score");
        bestScoreText.text = score.ToString();

        // splineGuide.Spline = pathList[currentPath];
        // splineGuide.Position = splineMove.Position;
        // splineGuide.Speed = speed * 3;
        StartCoroutine(delayGuide());
        directorCamera.Follow = listCars[currentLevel];
        directorCamera.LookAt = listCars[currentLevel];
    }

    IEnumerator delayGuide()
    {
        GuideControl();
        yield return new WaitForSeconds(2);
        StartCoroutine(delayGuide());
    }

    public void GuideControl()
    {
        splineGuide.gameObject.SetActive(false);
        splineGuide.Spline = pathList[currentPath];
        splineGuide.Position = splineMove.Position;
        splineGuide.Speed = speed * 2;
        splineGuide.gameObject.SetActive(true);
    }

    public void SwitchPath()
    {
        Debug.Log("Switch!");
        currentPath++;
        if(currentPath > pathList.Count - 1)
        {
            Win();
        }
        else
        {
            StartCoroutine(delaySwitchPath());
        }
    }

    IEnumerator delaySwitchPath()
    {
        splineMove.Speed = 0;
        splineMove.enabled = false;
        Vector3 des = pathList[currentPath].transform.GetChild(0).position;
        isEffect = false;
        isStartGame = false;
        transform.DOMoveZ(-3, 0.5f);
        yield return new WaitForSeconds(0.5f);
        transform.DOMove(new Vector3(des.x, des.y, transform.position.z), 1);
        yield return new WaitForSeconds(1);
        // transform.DOMoveZ(0, 0.5f);
        // yield return new WaitForSeconds(0.5f);
        isStartGame = true;
        isEffect = true;
        StartCoroutine(MoveEffect());
        splineMove.enabled = true;
        splineMove.Spline = pathList[currentPath];
        splineMove.Position = 0;
        splineMove.Speed = speed;

        // splineGuide.gameObject.SetActive(false);
        // splineGuide.Spline = pathList[currentPath];
        // splineGuide.Position = splineMove.Position;
        // splineGuide.Speed = speed * 3;
        // splineGuide.gameObject.SetActive(true);
    }

    private void Update()
    {
        if (isStartGame)
        {
            if (Input.GetMouseButton(0))
            {
                transform.GetChild(0).DOKill();
                transform.GetChild(0).DOMoveZ(0, 0.1f);
                splineMove.Speed = speed;
                if(!isEffect)
                {
                    StartCoroutine(MoveEffect());
                }
                isEffect = true;
                // splineGuide.Spline = pathList[currentPath];
                // splineGuide.Position = splineMove.Position;
                // splineGuide.Speed = 200;
            }
            if (Input.GetMouseButtonUp(0))
            {
                isEffect = false;
                splineMove.Speed = 0;
                transform.GetChild(0).DOMoveZ(-3, 0.5f);
                // splineGuide.Speed = 0;
            }
        }
    }

    IEnumerator MoveEffect()
    {
        yield return new WaitForSeconds(0.02f);
        if(isEffect)
        {
            var particle = Instantiate(effect, transform.position, Quaternion.identity);
            foreach (RegionGenerator region in LevelManager.Instance.activeRegions)
                region.Cut(transform.position, radius);
            StartCoroutine(MoveEffect());
        }
    }

    IEnumerator PlusEffect(Vector3 pos)
    {
        maxPlusEffect++;
        if (!UnityEngine.iOS.Device.generation.ToString().Contains("5"))
        {
            MMVibrationManager.Haptic(HapticTypes.LightImpact);
        }
        var plusVar = Instantiate(plusVarPrefab);
        plusVar.transform.SetParent(canvas.transform);
        plusVar.transform.localScale = new Vector3(1, 1, 1);
        //plusVar.transform.position = worldToUISpace(canvas, pos);
        plusVar.transform.position = new Vector3(pos.x + Random.Range(-20,20), pos.y + Random.Range(-50, 0), pos.z);
        plusVar.SetActive(true);
        plusVar.transform.DOMoveY(plusVar.transform.position.y + Random.Range(50, 90), 0.5f);
        Destroy(plusVar, 0.5f);
        yield return new WaitForSeconds(0.01f);
        maxPlusEffect--;
    }

    public Vector3 worldToUISpace(Canvas parentCanvas, Vector3 worldPos)
    {
        Vector3 screenPos = Camera.main.WorldToScreenPoint(worldPos);
        Vector2 movePos;
        RectTransformUtility.ScreenPointToLocalPointInRectangle(parentCanvas.transform as RectTransform, screenPos, parentCanvas.worldCamera, out movePos);
        return parentCanvas.transform.TransformPoint(movePos);
    }

    public void ButtonStartGame()
    {
        startGameMenu.SetActive(false);
        inGameMenu.SetActive(true);
        walls.GetComponent<MeshCollider>().sharedMesh = walls.GetComponent<MeshFilter>().sharedMesh;
        StartCoroutine(delayPlay());
    }

    IEnumerator delayPlay()
    {
        animator.Play("closeup");
        yield return new WaitForSeconds(2);
        splash.gameObject.SetActive(true);
        splash.DOFade(0, 1f);
        Camera.main.GetComponent<CinemachineBrain>().enabled = false;
        Camera.main.transform.position = new Vector3(0, 0, -12);
        Camera.main.transform.eulerAngles = Vector3.zero;
        isStartGame = true;
        Camera.main.GetComponent<CameraRatio>().Start();
    }

    public void Win()
    {
        if (isStartGame)
        {
            splineGuide.Speed = 0;
            isEffect = false;
            losePanel.SetActive(false);
            isStartGame = false;
            conffetiSpawn =  Instantiate(conffeti);
            currentLevel++;
            maxLevel = levelSizes.Count - 1;
            if (currentLevel > maxLevel)
            {
                currentLevel = 0;
            }
            PlayerPrefs.SetInt("currentLevel", currentLevel);
            StartCoroutine(delayWin());
        }
    }

    IEnumerator delayWin()
    {
        ball.transform.position = new Vector3(transform.position.x, transform.position.y, 1);
        ball.SetActive(true);
        // for(int i = 0; i < 300; i++)
        // {
        //     yield return new WaitForSeconds(0.001f);
        //     var balls = Instantiate(ball, new Vector3(transform.position.x, transform.position.y, 1), Quaternion.identity);
        // }
        int count = 0;
        foreach (var item in pathList)
        {
            int total = item.transform.childCount;
            foreach (Transform child in item.transform)
            {
                if (count < total / 100)
                {
                    count++;
                }
                else
                {
                    yield return new WaitForSeconds(0.001f);
                    var balls = Instantiate(ball, new Vector3(child.position.x, child.position.y, 1), Quaternion.identity);
                    count = 0;
                }
            }
        }
        yield return new WaitForSeconds(2);
        winPanel.SetActive(true);
    }

    public void Lose()
    {
        if (isStartGame)
        {
            isStartGame = false;
            losePanel.SetActive(true);
        }
    }

    public void LoadScene()
    {
        SceneManager.LoadScene(0);
    }

    public void OnChangeMap()
    {
        if (levelInput != null)
        {
            int level = int.Parse(levelInput.text.ToString());
            maxLevel = levelSizes.Count - 1;
            if (level <= maxLevel)
            {
                Debug.Log(level);
                PlayerPrefs.SetInt("currentLevel", level);
                SceneManager.LoadScene(0);
            }
        }
    }

    public void ButtonNextLevel()
    {
        isStartGame = true;
        Win();
        SceneManager.LoadScene(0);
    }

    public void delayChangeFogColor()
    {
        DOTween.To(() => RenderSettings.fogColor, x => RenderSettings.fogColor = x, themeColor, 5);
    }
}
